# Wordpress Theme from Scratch 

Build on the official Bootstrap blog starter template to learn WordPress.

[Based on the tutorial](https://www.taniarascia.com/developing-a-wordpress-theme-from-scratch/)

[Files pulled from repository](https://github.com/taniarascia/bootstrapblog)
