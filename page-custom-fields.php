<?php get_header(); ?>

<div class="row">

	<div class="col-sm-12">

	<?php
	$args = array(
		'post_type' => 'custom_fields_post',
	);
	$scratch_loop = new WP_Query( $args );
	if ( $scratch_loop->have_posts() ) :
		while ( $scratch_loop->have_posts() ) :
			$scratch_loop->the_post();
	$meta = get_post_meta( $post->ID, 'custom_fields', true );
	?>

	<h1>Title</h1>
	<?php the_title(); ?>

	<h4>Content</h4>
	<?php the_content(); ?>

	<p>Excerpt</p>
	<?php the_excerpt(); ?>

	<h5>Text Input</h5>
	<?php
	if ( is_array( $meta ) && isset( $meta['text'] ) ) {
		echo $meta['text'];
	}
	?>

	<h5>Textarea</h5>
	<?php
	if ( is_array( $meta ) && isset( $meta['textarea'] ) ) {
		echo $meta['textarea'];
	}
	?>

	<h5>Checkbox</h5>
	<?php
	if ( is_array( $meta ) && isset( $meta['checkbox'] ) ) {
		if ( $meta['checkbox'] === 'checkbox' ) {
		?>
			Checkbox is checked.
		<?php
		} else {
		?>
			Checkbox is not checked.
		<?php
		}
	}
	?>

	<h5>Select Menu</h5>
	<p>The actual value selected.</p>
	<?php
	if ( is_array( $meta ) && isset( $meta['select'] ) ) {
		echo $meta['select'];
	}
	?>
	<p>Switch statement for options.</p>
	<?php
	if ( is_array( $meta ) && isset( $meta['select'] ) ) {
		switch ( $meta['select'] ) {
			case 'option-one':
				echo 'Option One';
				break;
			case 'option-two':
				echo 'Option Two';
				break;
			default:
				echo 'No option selected';
				break;
		}
	}
	?>

	<h5>Image</h5>
	<img src="
	<?php
	if ( is_array( $meta ) && isset( $meta['image'] ) ) {
		echo $meta['image'];
	}
	?>
	">

	<?php
	endwhile; endif;
	wp_reset_postdata();
	?>

	</div><!-- /.col-->

</div><!-- /.row -->

<?php get_footer(); ?>
