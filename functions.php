<?php

// Adds scripts and stylesheets.
function scratch_scripts() {
	wp_enqueue_style(
		'bootstrap_css',
		get_template_directory_uri() . '/lib/bootstrap.min.css',
		array(),
		null
	);
	wp_enqueue_style(
		'blog_css',
		get_template_directory_uri() . '/assets/css/main.css'
	);
	wp_enqueue_script(
		'bootstrap_js',
		get_template_directory_uri() . '/lib/bootstrap.min.js',
		array( 'jquery' ),
		null,
		true
	);
}
add_action( 'wp_enqueue_scripts', 'scratch_scripts' );

// Adds Google Fonts.
function scratch_google_fonts() {
	wp_register_style(
		'OpenSans',
		'https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800'
	);
	wp_enqueue_style(
		'OpenSans'
	);
}
add_action( 'wp_print_styles', 'scratch_google_fonts' );

// Adds header WordPress titles.
add_theme_support( 'title-tag' );

// Adds custom settings in admin menu.
function custom_settings_add_menu() {
	add_menu_page(
		'Custom Settings',
		'Custom Settings',
		'manage_options',
		'custom-settings',
		'custom_settings_page',
		null,
		99
	);
}
add_action( 'admin_menu', 'custom_settings_add_menu' );

// Creates custom settings basic page.
function custom_settings_page() {
	?>
	<div class="wrap">
		<h1>Custom Settings</h1>
		<form action="options.php" method="post">
			<?php
				settings_fields( 'section' );
				do_settings_sections( 'theme-options' );
				submit_button();
			?>
		</form>
	</div>
	<?php
}

// Adds Twitter input field in custom settings page.
function setting_twitter() {
	?>
	<input
		id="twitter"
		type="text"
		name="twitter"
		value="<?php echo get_option( 'twitter' ); ?>"
	>
	<?php
}

// Adds Github input field in custom settings page.
function setting_github() {
	?>
		<input
			id="github"
			type="text"
			name="github"
			value="<?php echo get_option( 'github' ); ?>"
		>
	<?php
}

// Adds Facebook input field in custom settings page.
function setting_facebook() {
	?>
		<input
			id="facebook"
			type="text"
			name="facebook"
			value="<?php echo get_option( 'facebook' ); ?>"
		>
	<?php
}

// Adds page to show, accept and save the option fields.
function custom_settings_page_setup() {
	add_settings_section(
		'section',
		'All Settings',
		null,
		'theme-options'
	);
	add_settings_field(
		'twitter',
		'Twitter URL',
		'setting_twitter',
		'theme-options',
		'section'
	);
	add_settings_field(
		'github',
		'Github URL',
		'setting_github',
		'theme-options',
		'section'
	);
	add_settings_field(
		'facebook',
		'Facebook URL',
		'setting_facebook',
		'theme-options',
		'section'
	);
	register_setting( 'section', 'twitter' );
	register_setting( 'section', 'github' );
	register_setting( 'section', 'facebook' );
}
add_action( 'admin_init', 'custom_settings_page_setup' );

// Adds support for featured images in posts.
add_theme_support( 'post-thumbnails' );

// Adds Custom Post type
function create_scratch_custom_post() {
	register_post_type(
		'scratch-custom-post',
		array(
			'labels'      => array(
				'name'          => __( 'Custom Post' ),
				'singular_name' => __( 'Custom Post' ),
			),
			'public'      => true,
			'has_archive' => true,
			'supports'    => array(
				'title',
				'editor',
				'thumbnail',
				'custom-fields',
			),
		)
	);
}
add_action( 'init', 'create_scratch_custom_post' );

// Adds custom Scratch post with custom fields and meta boxes.
function create_post_custom_fields_post() {
	register_post_type(
		'custom_fields_post',
		array(
			'labels'       => array(
				'name'     => __( 'Custom Fields' ),
			),
			'public'       => true,
			'hierarchical' => true,
			'has_archive'  => true,
			'supports'     => array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
			),
			'taxonomies'   => array(
				'post_tag',
				'category',
			),
		)
	);
	register_taxonomy_for_object_type( 'category', 'custom_fields_post' );
	register_taxonomy_for_object_type( 'post_tag', 'custom_fields_post' );
};
add_action( 'init', 'create_post_custom_fields_post' );

// Adds meta box to custom Scratch post
function add_custom_fields_meta_box() {
	add_meta_box(
		'custom_fields_meta_box', // $id
		'Custom Fields', // $title
		'show_custom_fields_meta_box', // $callback
		'custom_fields_post', // $screen
		'normal', // $context
		'high' // $priority
	);
}
add_action( 'add_meta_boxes', 'add_custom_fields_meta_box' );

// Displays custom fields.
function show_custom_fields_meta_box() {
	global $post;
	$meta = get_post_meta( $post->ID, 'custom_fields', true );
	?>
	<input
		type="hidden"
		name="custom_meta_box_nonce"
		value="<?php echo wp_create_nonce( basename(__FILE__) ); ?>"
	>
	<!-- Custom fields go here. -->

	<p>
		<label for="custom_fields[text]">Input Text</label>
		<br>
		<input
			id="custom_fields[text]"
			class="regular-text"
			type="text"
			name="custom_fields[text]"
			value="
<?php if ( is_array( $meta ) && isset( $meta['text'] ) ) { echo $meta['text']; } ?>
			"
		>
	</p>

	<p>
		<label for="custom_fields[textarea]">Textarea</label>
		<br>
		<textarea
			id="custom_fields[textarea]"
			name="custom_fields[textarea]"
			rows="5"
			cols="30"
			style="width:500px;"
		>
<?php if ( is_array( $meta ) && isset( $meta['textarea'] ) ) { echo $meta['textarea']; } ?>
		</textarea>
	</p>

	<p>
		<label for="custom_fields[checkbox]">Checkbox
		<input
			type="checkbox"
			name="custom_fields[checkbox]"
			value="checkbox"
<?php if ( is_array( $meta ) && isset( $meta['checkbox'] ) ) { if ( $meta['checkbox'] === 'checkbox' ) echo 'checked'; } ?>
		>
		</label>
	</p>

	<p>
		<label for="custom_fields[select]">Select Menu</label>
		<br>
		<select id="custom_fields[select]" name="custom_fields[select]">
			<option
				value="option-one"
<?php if ( is_array( $meta ) && isset( $meta['select'] ) ) { selected( $meta['select'], 'option-one' ); } ?>
			>Option One</option>
			<option
				value="option-two"
<?php if ( is_array( $meta ) && isset( $meta['select'] ) ) { selected( $meta['select'], 'option-two' ); } ?>
			>Option Two</option>
		</select>
	</p>

	<p>
		<label for="custom_fields[image]">Image Upload</label>
		<br>
		<input
			id="custom_fields[image]"
			class="meta-image regular-text"
			type="text"
			name="custom_fields[image]"
			value="
<?php if ( is_array( $meta ) && isset( $meta['image'] ) ) { echo $meta['image']; } ?>
			"
		>
		<input class="button image-upload" type="button" value="Browse">
	</p>
	<div class="image-preview">
		<img
			src="
<?php if ( is_array( $meta ) && isset( $meta['image'] ) ) { echo $meta['image']; } ?>
			"
			alt="image preview"
			style="max-width: 250px;"
		>
	</div>
	<script>
	// Opens WordPress media gallery.
	// Code snippet modified from Foundation theme.
		jQuery(document).ready(function ($) {
		// Instantiates the variable that holds the media library frame.
			var meta_image_frame;
			// Runs when the image button is clicked.
			$('.image-upload').click(function (e) {
				// Get preview pane
				var meta_image_preview = $(this).parent().parent().children('.image-preview');
				// Prevents the default action from occuring.
				e.preventDefault();
				var meta_image = $(this).parent().children('.meta-image');
				// If the frame already exists, re-open it.
				if (meta_image_frame) {
					meta_image_frame.open();
					return;
				}
				// Sets up the media library frame
				meta_image_frame = wp.media.frames.meta_image_frame = wp.media({
					title: meta_image.title,
						button: {
						text: meta_image.button
					}
				});
				// Runs when an image is selected.
				meta_image_frame.on('select', function () {
					// Grabs the attachment selection and creates a JSON representation of the model.
					var media_attachment =
						meta_image_frame
							.state()
							.get('selection')
							.first()
							.toJSON();
					// Sends the attachment URL to our custom image input field.
					meta_image.val(media_attachment.url);
					meta_image_preview
						.children('img')
						.attr('src', media_attachment.url);
				});
				// Opens the media library frame.
				meta_image_frame.open();
			});
		});
	</script>
<?php
}

// Saves custom fields to a database.
function save_custom_fields_meta( $post_id ) {
	// Verify nonce.
	if (
		! wp_verify_nonce(
			$_POST['custom_meta_box_nonce'],
			basename( __FILE__ )
		)
	) {
		return $post_id;
	}
	// Check autosave.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return $post_id;
	}
	// Check permissions.
	if ( 'page' === $_POST['post_type'] ) {
		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return $post_id;
		} elseif ( ! current_user_can( 'edit_post', $post_id ) ) {
			return $post_id;
		}
	}

	$old = get_post_meta( $post_id, 'custom_fields', true );
	$new = $_POST['custom_fields'];

	if ( $new && $new !== $old ) {
		update_post_meta( $post_id, 'custom_fields', $new );
	} elseif ( '' === $new && $old ) {
		delete_post_meta( $post_id, 'custom_fields', $old );
	}
}
add_action( 'save_post', 'save_custom_fields_meta' );
